import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { Typography, Container, Box } from '@material-ui/core';
import SectionLayout from '../../layouts/sections-layout';

const useStyles = makeStyles(() => ({
    contentBox: {
        display: 'flex'
    },
    left: {
        float: 'left',
        display: 'inline-block',
        marginRight: '2vw',
        textAlign: 'right',
        verticalAlign: 'top',
        width: '45vw',
        paddingLeft: '5vw'
    },
    right: {
        float: 'right',
        display: 'inline-block',
        marginLeft: '2vw',
        textAlign: 'left',
        verticalAlign: 'top',
        width: '45vw',
        paddingRight: '5vw'
    }
}));

export default function EducationSection() {
    const classes = useStyles();

    return (
        <SectionLayout title="EDUCACIÓN" sectionId="education">
            <Container className={classes.contentBox}>
                <Box className={classes.left}>
                    <Typography variant='h6'>Some education title</Typography>
                    <Typography variant='body1'>01/01/1979</Typography>
                </Box>
                <Box className={classes.right}>
                    <Typography variant='h6'>Education description</Typography>
                    <Typography variant='body1'>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</Typography>
                    <Typography variant='body1'>Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</Typography>
                </Box>
            </Container>
            <Container className={classes.contentBox}>
                <Box className={classes.left}>
                    <Typography variant='h6'>Some education title</Typography>
                    <Typography variant='body1'>01/01/1979</Typography>
                </Box>
                <Box className={classes.right}>
                    <Typography variant='h6'>Education description</Typography>
                    <Typography variant='body1'>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</Typography>
                    <Typography variant='body1'>Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</Typography>
                </Box>
            </Container>
            <Container className={classes.contentBox}>
                <Box className={classes.left}>
                    <Typography variant='h6'>Some education title</Typography>
                    <Typography variant='body1'>01/01/1979</Typography>
                </Box>
                <Box className={classes.right}>
                    <Typography variant='h6'>Education description</Typography>
                    <Typography variant='body1'>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</Typography>
                    <Typography variant='body1'>Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</Typography>
                </Box>
            </Container>
        </SectionLayout>

    )
}