import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { Container, Box } from '@material-ui/core';
import avatarImg from '../../assets/images/avatar.jpg';

const useStyles = makeStyles((theme) => ({
    root: {
        position: 'relative',
        marginTop: '5vw',
        display: 'inline-block'
    },
    image: {
        width: '250px',
        borderRadius: '50%'
    },
    boxLeft: {
        [theme.breakpoints.up('md')]: {
            float: 'left',
            width: '45%',
            textAlign: 'right'
        },
        margin: '0 auto',
        textAlign: 'center'
    },
    boxRight: {
        [theme.breakpoints.up('md')]: {
            float: 'right',
            width: '45%',
            textAlign: 'left'
        },
        margin: '0 auto',
        textAlign: 'center'
    },
}));

export default function MainSection() {
    const classes = useStyles();

    return (
        <Container id="main" className={classes.root} >
            <Box className={classes.boxLeft} data-aos="fade-right">
                <img id="avatar" className={classes.image} src={avatarImg} alt='avatar' />
            </Box>
            <Box className={classes.boxRight} data-aos="fade-left">
                <h2 style={{ marginTop: 0 }}>Jakson Saxon</h2>
                <h3>Ocupación del payo</h3>
                <h4>Direccion: C/ Piruleta nº 69</h4>
                <h4>Telefono: 666666666</h4>
                <h4>Email: email@falso.com</h4>
                <h4>Nacimiento: 01/01/0001</h4>
            </Box>
        </Container>
    )
}