/* eslint-disable no-undef */
import React from 'react';
import { AnchorLink } from "gatsby-plugin-anchor-links";

import TypeIt from "typeit-react";

import Typography from '@material-ui/core/Typography';
import { AppBar, Toolbar, Button, makeStyles } from '@material-ui/core';

const useStyle = makeStyles((theme) => ({
    navbar: {
        textAlign: 'right',
        width: '100%',
        paddingTop: '1vw',
        [theme.breakpoints.down('md')]: {
            display: 'none'
        }
    },
    nameDiv: {
        [theme.breakpoints.down('md')]: {
            textAlign: 'center',
            width: '100%'
        },
        width: '40%'
    },
    name: {
        color: [theme.palette.primary[500]]
    }
}));


export default function HeaderSection() {
    const classes = useStyle();

    return (
        <AppBar position="static" >
            <Toolbar>
                <div className={classes.nameDiv}>
                    <AnchorLink to="/#home">
                        <TypeIt element={'h1'} className={classes.name}>
                            Jackson Saxon
                        </TypeIt>
                    </AnchorLink>
                </div>
                <div className={classes.navbar}>
                    <Button>
                        <AnchorLink to="/#experience">
                            <Typography variant="h6">Experiencia</Typography>
                        </AnchorLink>
                    </Button>
                    <Button>
                        <AnchorLink to="/#education">
                            <Typography variant="h6">Educación</Typography>
                        </AnchorLink>
                    </Button>
                    <Button>
                        <AnchorLink to="/#skills">
                            <Typography variant="h6">Habilidades</Typography>
                        </AnchorLink>
                    </Button>
                    <Button>
                        <AnchorLink to="/#contact">
                            <Typography variant="h6">Contacto</Typography>
                        </AnchorLink>
                    </Button>
                </div>
            </Toolbar>
        </AppBar >
    )
}