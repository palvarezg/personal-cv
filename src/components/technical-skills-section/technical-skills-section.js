import React from 'react'
import { List, ListItem, ListItemText, Collapse, Typography, Slider, } from '@material-ui/core';
import { ExpandLess, ExpandMore } from '@material-ui/icons';
import { makeStyles } from '@material-ui/core/styles';
import PropTypes from "prop-types";

import SectionLayout from '../../layouts/sections-layout';

let classes;

const useStyles = makeStyles((theme) => ({
    root: {
        width: '100%',
        maxWidth: 360,
        backgroundColor: theme.palette.background.paper,
    },
    nested: {
        paddingLeft: theme.spacing(4),
    },
    slider: {
        width: '50vw',
        marginLeft: '15px'
    },
    sliderTitle: {
        float: 'left',
        width: '150px'
    }
}));

// PROBAR CON https://material-ui.com/es/components/expansion-panels/

export default function TechnicalSkillsSection() {
    classes = useStyles();

    const webSkills = [{ title: "Angular 2", value: 70 },
    { title: "Gatsby", value: 50 },
    { title: "React", value: 40 },
    { title: "JS", value: 50 }];

    const desktopSkills = [{ title: "WPF", value: 70 }];

    const mobileSkills = [{ title: "Android", value: 50 }];

    const idesSkills = [{ title: "Visual Studio 19", value: 80 },
    { title: "VS Code", value: 90 },
    { title: "Visual Studio 17", value: 80 },
    { title: "Android Studio", value: 60 }]

    return (
        <SectionLayout title="HABILIDADES TÉCNICAS" sectionId="skills">
            <List>
                <SkillsGroup title="Web" skills={webSkills}></SkillsGroup>
                <SkillsGroup title="Escritorio" skills={desktopSkills}></SkillsGroup>
                <SkillsGroup title="Movil" skills={mobileSkills}></SkillsGroup>
                <SkillsGroup title="IDEs" skills={idesSkills}></SkillsGroup>
            </List>
        </SectionLayout>
    );
}

function SkillsGroup(props) {
    const [open, setOpen] = React.useState(false);

    const handleClick = () => {
        setOpen(!open);
    };

    return (<div>
        <ListItem button onClick={handleClick}>
            <ListItemText>
                <Typography variant="h5">{props.title}{open ? <ExpandLess /> : <ExpandMore />}</Typography>
            </ListItemText>
        </ListItem>
        <Collapse in={open} timeout="auto" unmountOnExit>
            <List component="div" disablePadding>
                {props.skills.map(skill =>
                    <SkillComponent key={skill.title} title={skill.title} value={skill.value}>
                    </SkillComponent>
                )}
            </List>
        </Collapse>
    </div>)
}

function SkillComponent(props) {
    return (<ListItem button className={classes.nested}>
        <ListItemText>
            <Typography gutterBottom className={classes.sliderTitle}>{props.title}</Typography>
            <Slider defaultValue={props.value} min={10} max={100} className={classes.slider} disabled />
        </ListItemText>
    </ListItem>)
}

SkillsGroup.propTypes = {
    title: PropTypes.string,
    skills: PropTypes.arrayOf(PropTypes.shape(
        {
            title: PropTypes.string,
            value: PropTypes.number
        })
    )
}

SkillComponent.propTypes = {
    title: PropTypes.string,
    value: PropTypes.number,
    classes: PropTypes.any
}