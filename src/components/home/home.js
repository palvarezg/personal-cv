import React from 'react';
import { makeStyles, Fab, Typography, } from '@material-ui/core';
import { AnchorLink } from 'gatsby-plugin-anchor-links';

import Header from '../header/header';
import MainSection from '../main-section/main-section';
import ExperienceSection from '../experience-section/experience-section';
import EducationSection from '../education-section/education-section';
import TechnicalSkillsSection from '../technical-skills-section/technical-skills-section';
import ContactSection from '../contact-section/contact-section';
import Footer from '../footer/footer';

const useStyles = makeStyles((theme) => ({
    root: {
        [theme.breakpoints.up('md')]: {
            marginLeft: '15vw',
            marginRight: '15vw',
        },
        marginLeft: '5vw',
        marginRight: '5vw',
        backgroundColor: theme.palette.secondary[900]
    },
    link: {
        color: theme.palette.primary.contrastText
    }
}));

export default function Home() {
    const classes = useStyles();
    return (<div>
        <div className={classes.root} id="home">
            <Header ></Header>
            <MainSection></MainSection>
            <ExperienceSection></ExperienceSection>
            <EducationSection></EducationSection>
            <TechnicalSkillsSection></TechnicalSkillsSection>
            <ContactSection></ContactSection>
            <Fab>
                <AnchorLink to="/#home">
                    <Typography variant="body2" className={classes.link}>
                        PA&apos;
                        RRIBA
                </Typography>
                </AnchorLink>
            </Fab>
        </div>
        <Footer></Footer>
    </div>)
}