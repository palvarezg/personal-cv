import React from 'react';
import { TextField, makeStyles, Button } from '@material-ui/core';
import SectionLayout from '../../layouts/sections-layout';

const useStyles = makeStyles((theme) => ({
    button: {
        margin: '1vw',
        float: 'left',
        height: '10vw',
        [theme.breakpoints.up('md')]: {
            width: '10vw',
        }
    },
    grid: {
        [theme.breakpoints.up('md')]: {
            paddingLeft: '5vw',
            paddingRight: '5vw',
            gridTemplateColumns: '1fr 1fr',
            gridTemplateRows: '0.5fr 0.5fr 0.5fr 1fr 0.5fr'
        },
        paddingLeft: '1vw',
        paddingRight: '1vw',
        display: 'grid',
        gridTemplateColumns: '1fr',
        gridTemplateRows: 'auto'
    },
    longBox: {
        [theme.breakpoints.up('md')]: {
            gridColumnStart: 'span 2'
        }
    }
}));

export default function ContactSection() {
    const classes = useStyles();
    return (
        <SectionLayout title="CONTACTO" sectionId="contact">
            <form className={classes.grid}>
                <TextField label="Nombre" variant="outlined" required />
                <TextField label="Apellidos" variant="outlined" required />
                <TextField label="Email" variant="outlined" required />
                <TextField label="Telefono" variant="outlined" />
                <TextField label="Asunto" variant="outlined" required className={classes.longBox} />
                <TextField label="Contenido" variant="outlined" required multiline rows='5' className={classes.longBox} />
                <Button variant="contained" className={classes.button}>ENVIAR</Button>
            </form>
        </SectionLayout>
    );
}