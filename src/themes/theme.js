import { createMuiTheme } from "@material-ui/core";
import { grey, purple } from "@material-ui/core/colors";

const theme = createMuiTheme({
    typography: {
        fontFamily: '\'Fira Code\', monospace,\'Source Code Pro\', monospace',
    },
    palette: {
        type: 'dark',
        primary: purple,
        secondary: grey,
        background: {
            default: grey[900]
        }
    },
    overrides: {
        MuiDivider: {
            root: {
                backgroundColor: purple[500],
                marginTop: '1vw',
                marginBottom: '1vw'
            }
        },
        MuiTypography: {
            h2: {
                color: purple[500]
            },
            h4: {
                color: purple[500]
            },
            h6: {
                color: purple[500]
            }
        },
        MuiOutlinedInput: {
            root: {
                borderRadius: '0px'
            }
        },
        MuiFormControl: {
            root: {
                marginTop: '1vw',
                marginBottom: '1vw',
                marginLeft: '1vw',
                marginRight: '1vw',

            }
        },
        MuiFab: {
            root: {
                color: grey[900],
                backgroundColor: purple[500],
                top: 'auto',
                right: '20px',
                bottom: '20px',
                left: 'auto',
                position: 'fixed'
            },
        },
        MuiAppBar: {
            root: {
                boxShadow: 'none'
            },
            positionSticky: true
        },
        MuiToolbar: {
            root: {
                alignItems: 'flex-start',
                backgroundColor: grey[900]
            }
        }
    }
});

export default theme;