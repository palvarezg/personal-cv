import React from 'react'
import { makeStyles } from '@material-ui/core/styles';
import { Divider, Typography } from '@material-ui/core';

const useStyles = makeStyles(() => ({
    root: {
        position: 'relative',
        textAlign: 'center',
        marginTop: '5vw'
    }
}));

// eslint-disable-next-line react/prop-types
export default function SectionLayout({ children, title, sectionId }) {
    const classes = useStyles();
    return (
        <div id={sectionId} className={classes.root} data-aos="flip-up">
            <Typography variant='h5'>{title}</Typography>
            <Divider orientation='horizontal' />
            {children}
        </div>
    );
}