import React from 'react';
import { ThemeProvider } from '@material-ui/core';
import CssBaseline from "@material-ui/core/CssBaseline";

import theme from '../themes/theme.js';
import Home from '../components/home/home';


export default function Index () {
    return (<ThemeProvider theme={theme}>
        <CssBaseline />
        <Home></Home>
    </ThemeProvider>)
}