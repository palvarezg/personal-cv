/**
 * Implement Gatsby's SSR (Server Side Rendering) APIs in this file.
 *
 * See: https://www.gatsbyjs.org/docs/ssr-apis/
 */

import React from "react";

export const onRenderBody = ({ setHeadComponents, setPostBodyComponents }) => {
    setHeadComponents([
        <link rel="stylesheet" href="https://unpkg.com/aos@next/dist/aos.css" />
    ]);
    setPostBodyComponents([
        <script src="https://unpkg.com/aos@next/dist/aos.js"></script>,
        <script dangerouslySetInnerHTML={{
            __html: `
                AOS.init({
                    duration: 1000
                });
                `,
        }} />
    ]);
};